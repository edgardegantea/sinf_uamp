from django.db import models

# Create your models here.

NACIONALIDAD = (('Mexicano (a)', 'Mexicano (a)'),
                ('Español (a)', 'Español (a)'),
                ('Estadounidense', 'Estadounidense')
                )

RACTUAL = (('México', 'México'),
           ('España', 'España'),
           ('Estados Unidos', 'Estados Unidos'),
           ('Inglaterra', 'Inglaterra')
            )

class Autor(models.Model):
    nombre = models.CharField(verbose_name='Nombre', max_length=50)
    apellidoPaterno = models.CharField(verbose_name='Apellidos', max_length=50)
    nacionalidad = models.CharField(choices=NACIONALIDAD, verbose_name='Nacionalidad', max_length=14, default='Mexicano (a)')
    fechaNacimiento = models.DateField(verbose_name='Fecha de nacimiento')
    fechaFallecimiento = models.DateField(verbose_name='Fecha de fallecimiento', null=True, blank=True)
    residenciaActual = models.CharField(choices=RACTUAL, verbose_name='País de residencia actual', max_length=14, default='México')
